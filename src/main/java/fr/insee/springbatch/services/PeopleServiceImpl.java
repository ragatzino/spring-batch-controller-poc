package fr.insee.springbatch.services;

import fr.insee.springbatch.model.entity.People;
import fr.insee.springbatch.dao.repositories.PeopleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class PeopleServiceImpl implements PeopleService {
    private PeopleRepository peopleRepository;

    @Override
    public List<People> getAllPeople() {
        return peopleRepository.findAll();
    }

    @Override
    public People getPeopleById(long personId) {
        return peopleRepository.findByPersonId(personId);
    }

    @Autowired
    public void setPeopleRepository(PeopleRepository peopleRepository){
        this.peopleRepository=Objects.requireNonNull(peopleRepository,"peopleRepository is null");
    }
}
