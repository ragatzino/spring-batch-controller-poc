package fr.insee.springbatch.web.controller;

import fr.insee.springbatch.model.entity.People;
import fr.insee.springbatch.services.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
public class PeopleController {

    private PeopleService peopleService;

    @Autowired
    public void setPeopleService(PeopleService peopleService) {
        this.peopleService = Objects.requireNonNull(peopleService,"peopleService est null");
    }

    @GetMapping("/peoples")
    public List<People> getPeoples(){
        return peopleService.getAllPeople();
    }

    @GetMapping("/people/{id}")
    public People getPeopleById (@PathVariable long id){
        return peopleService.getPeopleById(id);
    }
}

